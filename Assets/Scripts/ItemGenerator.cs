﻿using UnityEngine;

public class ItemGenerator : MonoBehaviour
{
    [Tooltip("Шаг, через который спавнятся новые предметики(монетки/бонусы)")]
    [SerializeField]
    private float _spawnRateTime;
    [Tooltip("Задержка в начале игры, после которой начинает работать спавн предметов")]
    [SerializeField]
    private float _spawnCooldown;
    [Tooltip("Массив с предметами")]
    [SerializeField]
    private ItemBehaviour[] _items;
    
    // Update is called once per frame
    void FixedUpdate()
    {
        if(Time.time > _spawnCooldown)
        {
            int index = Random.Range(0, _items.Length);
            if(_items[index].SpawnChance() < Random.Range(0, 100))
            {
                return;
            }
            float offsetY = Random.Range(1, 8);
            GameObject item = Instantiate(_items[index].gameObject, gameObject.transform.position, Quaternion.Euler(_items[index].gameObject.transform.localEulerAngles), gameObject.transform);
            item.transform.position = new Vector3(gameObject.transform.position.x, offsetY, 0);
            _spawnCooldown = Time.time + _spawnRateTime;
        }
    }

}
