﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _jumpForce;

    private Rigidbody _rigidBody;
    private Animator _anim;

    // закончена ли игра
    private bool _gameOver;
    // коэффициент гравитации
    private int _gravityForce;
    // может ли герой прыгнуть
    private bool _canJump;
    // находится ли герой в возлухе
    private bool _inAir;

    // Start is called before the first frame update
    void Start()
    {
        _gravityForce = 1;
        _inAir = false;
        _canJump = false;
        _gameOver = false;
        _rigidBody = GetComponent<Rigidbody>();
        _anim = GetComponentInChildren<Animator>();
        _anim.SetBool("isJump", false);
        _anim.SetBool("isRunning", true);
    }

    // Update is called once per frame
    void Update()
    {
        if (_gameOver) return;
        
        _rigidBody.velocity = new Vector3(0, _rigidBody.velocity.y, 0);

        _inAir = Mathf.Abs(_rigidBody.velocity.y) > 0.2;
        _anim.SetBool("isJump", _inAir);
        _anim.SetBool("isRunning", !_inAir);
    }

    #region Public
    public void Reaction()
    {
        if (!_inAir)
        {
            // не можем прыгать - меняем гравитацию.
            if (!_canJump)
            {
                Physics.gravity *= -1;
                _gravityForce *= -1;

                // так делать не желательно - потанцевальный генератор багов - но я решился
                Invoke("FlipPlayerScaleY", 0.2f);
            }
            // иначе прыгаем
            else
            {
                Jump();
            }

        }
    }

    public void SetJump(bool value)
    {
        _canJump = value;
    }

    public void GameOver()
    {
        _gameOver = true;
        _rigidBody.useGravity = false;
        _rigidBody.isKinematic = true;
        _anim.SetBool("isDead", true);
    }

    #endregion

    #region Private
    // флипаем модель героя по оси X для ходьбы по потолку
    private void FlipPlayerScaleY()
    {
        gameObject.transform.localEulerAngles = new Vector3(_gravityForce > 0 ? 0 : 180, 0, 0);
    }

    // прыжок. все просто.
    private void Jump()
    {
        _rigidBody.AddForce(transform.up * _jumpForce, ForceMode.Impulse);
    }
    #endregion
}
