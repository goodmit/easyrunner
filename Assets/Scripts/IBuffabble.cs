﻿
public interface IBuffablle
{
    string Name();
    void Run();
    void Stop();
    float BuffTime();
}
