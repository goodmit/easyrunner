﻿using UnityEngine;

/// <summary>
/// Просто убивает игрока, когда тот забрел не в тот райончик
/// </summary>
public class TriggerKiller : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            GameController.Instance.GameOver();
        }
    }
    
}
