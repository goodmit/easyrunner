﻿using UnityEngine;

public class PlatformSpawner : MonoBehaviour
{
    [Tooltip("Коффициент появления дыр на линии. Чем он больше, тем больше дырок в платформах")]
    [Range(0, 1)]
    [SerializeField]
    private float _holeFrequency = 0.5f;

    [Tooltip("Префаб для платформы")]
    [SerializeField]
    private GameObject _platform;
    [Tooltip("Последняя заспавленная платформа. Должна быть всегда, иначе беда.")]
    [SerializeField]
    private GameObject _lastSpawned;
    [Tooltip("Максимальная длина платформы")]
    [SerializeField]
    private int maxPlatformsize = 5;
    private int minPlatformsize = 2;
    
    // Здесь мы передвигаем уехавшую за края экрана платформу на новое место
    public void ResetPlatform(GameObject platform)
    {
        platform.transform.localScale = new Vector3(Random.Range(minPlatformsize, maxPlatformsize + 1), 1, 1);
        float posX = _lastSpawned.transform.position.x + (_lastSpawned.transform.localScale.x / 2) + (platform.transform.localScale.x / 2 );
        platform.transform.position = new Vector3(posX - 0.3f + AddGap(), platform.transform.position.y, 0);
        _lastSpawned = platform;
    }

    // здесь добавляем дырку в полу.
    // ну или на потолке. кому как нравится
    private float AddGap()
    {
        if (_holeFrequency > Random.Range(0f, 1f))
        {
            return Random.Range(2, 3.0f);
        }
        return 0;
    }

}
