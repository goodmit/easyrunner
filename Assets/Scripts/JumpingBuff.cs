﻿using UnityEngine;

/// <summary>
/// Бафф, меняющий гравитацию на обычный прыжок
/// </summary>
public class JumpingBuff : MonoBehaviour, IBuffablle
{

    [SerializeField]
    private float _time;
    
    public string Name()
    {
        return "Прыжок";
    }

    public float BuffTime()
    {
        return _time;
    }

    public void Run()
    {
        GameController.Instance.SetGravitySwitcher(true);
    }

    public void Stop()
    {
        GameController.Instance.SetGravitySwitcher(false);
    }
}
