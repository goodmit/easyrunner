﻿using UnityEngine;

public class ScoreController : MonoBehaviour
{
    [Tooltip("Базовая величина прироста очков")]
    [SerializeField]
    private int baseScore = 1;

    //множитель от баффа. при старте всегда 1
    private int _buffForce = 1;

    public int score { get; private set; }
    public int hiscore { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        _buffForce = 1;
        score = 0;
        LoadData();
    }

    #region Public
    public void GameOver()
    {
        if (score > hiscore)
        {
            hiscore = score;
            SaveHiscore();
        }
    }

    public int GetScore()
    {
        return score;
    }

    public void IncreaseScore()
    {
        score += baseScore * _buffForce;
    }

    public void SetBuffForce(int newForce)
    {
        _buffForce = newForce;
    }
    #endregion

    #region Private

    private void SaveHiscore()
    {
        PlayerPrefs.SetInt("hiscore", hiscore);
    }

    private void LoadData()
    {
        hiscore = PlayerPrefs.GetInt("hiscore", 0);
    }
    #endregion
}
