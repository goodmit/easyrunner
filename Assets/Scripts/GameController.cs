﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Основной контроллер игры. Связывает между собой
/// большинство различных модулей.
/// </summary>
[RequireComponent(typeof(BuffController))]
[RequireComponent(typeof(ScoreController))]
[RequireComponent(typeof(UIController))]
public class GameController : Singleton<GameController>
{

    [SerializeField]
    private PlayerController _player;
    
    public float speedObjects;

    // нужно чтоб были доступны только для чтения
    public ScoreController scoreController { get; private set; }
    public BuffController buffController { get; private set; }

    private UIController _uiController;

    override protected void Awake()
    {
        base.Awake();
        // небольшая настройка физики. мне так захотелось
        Physics.gravity = new Vector3(0, -100f, 0);
    }

    void Start()
    {
        _uiController = GetComponent<UIController>();
        scoreController = GetComponent<ScoreController>();
        buffController = GetComponent<BuffController>();
        
    }

    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space)) {
            InteractWithPlayer();
        }
    }

    #region Public

    public void InteractWithPlayer()
    {
        _player.Reaction();
    }

    public void SetGravitySwitcher(bool value)
    {
        _uiController.SetJumpIconActive(value);
        _player.SetJump(value);
    }

    public void GameOver()
    {
        _player.GameOver();
        _uiController.ShowGameOverPanel();
        scoreController.GameOver();
    }

    public void ShowStatistics()
    {
        _uiController.ShowStatistics();
    }

    public void QuitApp()
    {
        Application.Quit();
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void CollectItem()
    {
        scoreController.IncreaseScore();        
    }

    #endregion
}
