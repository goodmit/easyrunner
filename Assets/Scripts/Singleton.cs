﻿using UnityEngine;

/// <summary>
/// Базовый класс для синглтонов
/// Создает экземпляр-одиночку в проекте.
/// </summary>
/// <typeparam name="T">Тип данных синглтона</typeparam>
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    public static T Instance => instance;

    protected static T instance;
    
    protected virtual void Awake() {
        if (Instance == null) {
            instance = GetComponent<T>();
        }
        else {
            Debug.LogError("Something went wrong. There should never be more than one instance of " + typeof(T));
        }
    }

}

