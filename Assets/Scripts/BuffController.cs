﻿using UnityEngine;

public class BuffController : MonoBehaviour
{
    public enum BuffType
    {
        NONE,
        DOUBLE_SCORE,
        JUMPING
    }

    [SerializeField]
    private JumpingBuff _jumpBuff;
    [SerializeField]
    private DoubleScoreBuff _doubleScoreBuff;
    [SerializeField]
    private string _name;
    private float _endTime;

    public string buffName { get; private set; }
    public int remainingTime { get; private set; }
    
    public IBuffablle _activeBuff { get; private set; }

    void Start()
    {
        buffName = _name;
    }

    // тут мы просто следим за временем баффа
    void FixedUpdate()
    {
        remainingTime = (int)(_endTime - Time.time);
        if (remainingTime <= 0)
        {
            remainingTime = 0;
            if (_activeBuff != null)
            {
                _activeBuff.Stop();
                buffName = _name;
            }

        }
    }

    // здесь мы активируем бафф
    public void RunBuff(BuffType type)
    {
        if (_activeBuff != null)
        {
            _activeBuff.Stop();
        }
        switch (type)
        {
            case BuffType.JUMPING:       
                _endTime = Time.time + _jumpBuff.BuffTime();
                _activeBuff = _jumpBuff;
                break;
            case BuffType.DOUBLE_SCORE:
                _endTime = Time.time + _doubleScoreBuff.BuffTime();
                _activeBuff = _doubleScoreBuff;
                break;
        }
        buffName = _activeBuff.Name();
        _activeBuff.Run();
    }
}
