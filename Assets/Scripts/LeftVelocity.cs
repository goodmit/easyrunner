﻿using UnityEngine;

/// <summary>
/// Этот скрипт просто заставляет объект непрерывно двигаться влево. 
/// А потом самых левонастроенных отправляет на перевоспитание к родителю.
/// </summary>
public class LeftVelocity : MonoBehaviour
{

    private float _speed;
    private Rigidbody _rigidBody;

    void Start()
    {
        _rigidBody = gameObject.GetComponentInChildren<Rigidbody>();
        _speed = GameController.Instance.speedObjects;
    }

    void FixedUpdate()
    {
        transform.Translate(Vector3.left * _speed * Time.deltaTime);
        if (transform.position.x < -15)
        {
            if (transform.tag == "Ground")
            {
                PlatformSpawner spawner = gameObject.GetComponentInParent<PlatformSpawner>();
                if(spawner != null)
                {
                    spawner.ResetPlatform(gameObject);
                }
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
