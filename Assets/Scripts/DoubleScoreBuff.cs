﻿using UnityEngine;

/// <summary>
/// Бафф, который дает бонус удвоенного получения очков
/// </summary>
public class DoubleScoreBuff : MonoBehaviour, IBuffablle
{
    [SerializeField]
    private int _baseScore;
    [SerializeField]
    private int _buffedScore;
    [SerializeField]
    private float _time;
    
    public string Name()
    {
        return "Двойные очки";
    }
   
    public float BuffTime()
    {
        return _time;
    }

    public void Run()
    {
        GameController.Instance.scoreController.SetBuffForce(_buffedScore);
    }

    public void Stop()
    {
        GameController.Instance.scoreController.SetBuffForce(_baseScore);
    }
}
