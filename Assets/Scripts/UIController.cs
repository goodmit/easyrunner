﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Этот класс отвечает за UI
/// </summary>
public class UIController : MonoBehaviour
{
    [Header("Панели")]
    [Tooltip("Панель при проигрыше")]
    [SerializeField]
    private GameObject _gameOverPanel;
    [Tooltip("Панель очков")]
    [SerializeField]
    private GameObject _scorePanel;
    [Tooltip("Панель в игре")]
    [SerializeField]
    private GameObject _gamePanel;

    [Header("Интерфейс в игре")]
    [Tooltip("Отображает количество набранных очков во время игры")]
    [SerializeField]
    private Text _gameScoreTxt;
    [Tooltip("Отображает оставшееся время бафа")]
    [SerializeField]
    private Text _buffTxt;
    [Tooltip("Иконка Прыжка")]
    [SerializeField]
    private Image _imgJump;
    [Tooltip("Иконка гравитации")]
    [SerializeField]
    private Image _imgGravy;

    [Header("")]
    [Tooltip("Отображает количество набранных очков")]
    [SerializeField]
    private Text _scoreTxt;
    [Tooltip("Отображает лучший счет")]
    [SerializeField]
    private Text _hiscoreTxt;
    
    void Start()
    {
        _gameOverPanel.SetActive(false);
        _scorePanel.SetActive(false);
        _gamePanel.SetActive(true);
        SetJumpIconActive(false);
    }

    void FixedUpdate()
    {
        UpdateData();
    }
    
    public void UpdateData()
    {
        _buffTxt.text = GameController.Instance.buffController.buffName + ": " + GameController.Instance.buffController.remainingTime.ToString();
        _gameScoreTxt.text = "Очки: " + GameController.Instance.scoreController.score.ToString();
        _scoreTxt.text = "Твой счёт: " + GameController.Instance.scoreController.score.ToString();
        _hiscoreTxt.text = "Лучший счёт: " + GameController.Instance.scoreController.hiscore.ToString();
    }

    public void SetJumpIconActive(bool value)
    {
        _imgJump.enabled = value;
        _imgGravy.enabled = !_imgJump.enabled;
    }

    public void ShowGameOverPanel(bool value = true)
    {
        _gamePanel.SetActive(!value);
        _gameOverPanel.SetActive(value);
    }

    public void ShowStatistics()
    {
        UpdateData();
        _scorePanel.SetActive(true);
        _gameOverPanel.SetActive(false);
        _gamePanel.SetActive(false);
    }
}
