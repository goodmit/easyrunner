﻿using UnityEngine;

/// <summary>
/// Определяет поведения предмета - коллизии, взаимодействие...
/// </summary>
public class ItemBehaviour : MonoBehaviour, IInteractable
{
    [Tooltip("Тип бонуса, привязанный к этому предмету")]
    [SerializeField]
    private BuffController.BuffType _buff;
    [Tooltip("Шанс спавна для этого типа предмета")]
    [Range(0, 100)]
    [SerializeField]
    private int _spawnChance;

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x < -10)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Interact();
            Destroy(gameObject);
        }
    }

    #region Public
    public void Interact()
    {
        if (_buff != BuffController.BuffType.NONE)
        {
            GameController.Instance.buffController.RunBuff(_buff);
        }
        else
        {
            GameController.Instance.CollectItem();
        }
    }

    public int SpawnChance()
    {
        return _spawnChance;
    }

    #endregion
}
