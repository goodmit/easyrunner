﻿using UnityEngine;

public class HideEvent : MonoBehaviour
{
    // событие для завершения анимации окна проигрыша
    public void HideGameOverPanel()
    {
        GameController.Instance.ShowStatistics();
    }
    
}
